# Nixos on gitlab ci!

## Are you going to upstream this?
I hope so. I prefer to now experiment in my own repository.

The current `gitlab-runner` module in NixOS is severely broken,
as it modifies the `config.toml` file, which is not a configuration
file, but the file that `gitlab-ci` uses to store internal state,
and thus is modified heavily by the `gitlab-runner` daemon.
If multiple runners are registered on one machine, they all modify
this same file. And update their tokens and config parameters in this
file dynamically. It should be avoided to touch it by hand! [Citation needed]

Because the current nixos module generates this file directly,
we can not add multiple runners easily, and there is no auto-registration
option.

By delegating this task to the `gitlab-runner register` command, we get
support for running multiple runners in parallel, and we get automatic
declarative registration and de-registration of runners.


I would like to make this module the default `gitlab-runner` module in NixOS
in the future, but it still needs some more development:

*  We need to support plain docker builds, for people who  have non-nix projects
*  Need to support adding multiple runners
*  Figure out if we can keep the nixos config backwards compatible. But I think the
   answer is a NO, unfortunately. Because the current module's approach is wrong.

## Installation

Add this to your nixos configuration:

```
imports = [ ./gitlab-runner.nix];


services.gitlab-runner2.enable = true;
services.gitlab-runner2.registrationConfigFile = "/var/lib/secrets/mysecret"


```

The registration config file contains the secretes used to connect to
your gitlab instance. You should provision it manually, or
using a tool like `nixops` or `vault`.

The content of the file should be as follows:

```
CI_SERVER_URL=<CI server URL>
REGISTRATION_TOKEN=<registration secret>

```

And you're all set! You can now build Nix projects using
all nix commands. Check
out the `.gitlab-ci.yml` for an example

The module will
* Automatically register on startup
* Automatically deregister at shutdown
* Gracefully finish all builds


## How does it work

A minimal container (`alpine`) gets the `nix-daemon` of the host
system bind-mounted into the container. This means
that all `nix` commands inside the container are delegated to the host,
which maintains the `/nix/store`
Build artifacts are again visible in the container because
`/nix/store` is also bind-mounted in the container. 
This means, that it will only rebuild what is needed.

This way, we kind of trick gitlab to actually let the builds happen
on the host system. However, `nix` already takes care of isolation
and integrity of the `/nix/store`, so modulo bugs in the `nix-daemon`
protocol, this should be similar in safety as docker.


## Pros
* Caching between builds
* Fast rebuilds
* Sharing between builds, _even between different gitlab projects_.


## Cons
The container can reach the host `/nix/store`, read-only... It
can't modify it directly, but it can view all its contents.  
This might be a problem if you are running `gitlab-runner` on a shared
system where secrets are stored in the nix store (Which you shouldn't do!!)
and you are running arbitrary code inside your CI.

However, if you own the build servers yourself, there is little risk.
Also, you can read build artifacts of other builds... So again, using
this for a shared build farm is probably not a smart idea, but it's a start.





